package routes

import (
	"github.com/gorilla/mux"
	"loco-app/domain/transactions"
	"loco-app/httpapi/handlers"
	txnStore "loco-app/store/transactions"
	"net/http"
)

func NewRouter(txnService handlers.TransactionService) *mux.Router {
	router := mux.NewRouter()

	router.HandleFunc("/transaction_service/transactions/{transaction_id}", handlers.CreateOrUpdate(txnService)).Methods("PUT")
	router.HandleFunc("/transaction_service/transactions/{transaction_id}", handlers.GetById(txnService)).Methods("GET")
	router.HandleFunc("/transaction_service/transactions/types/{type}", handlers.ListByType(txnService)).Methods("GET")
	router.HandleFunc("/transaction_service/transactions/sum/{transaction_id}", handlers.GetSum(txnService)).Methods("GET")

	return router
}

func main() {
	txnsStore := txnStore.NewStore()
	txnService := transactions.NewService(txnsStore)

	_ = http.ListenAndServe(":8080", NewRouter(txnService))
}
