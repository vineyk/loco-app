package handlers

import (
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"io/ioutil"
	"loco-app/domain/transactions"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

type MockService struct {
	createOrUpdate func(txn transactions.Transaction) (*transactions.Transaction, error)
	getById        func(txnId int64) (*transactions.Transaction, error)
	getSum         func(txnId int64) float64
	listByType     func(txnType string) []transactions.Transaction
}

func (mS *MockService) CreateOrUpdate(txn transactions.Transaction) (*transactions.Transaction, error) {
	return mS.createOrUpdate(txn)
}

func (mS *MockService) GetById(txnId int64) (*transactions.Transaction, error) {
	return mS.getById(txnId)
}

func (mS *MockService) GetSum(txnId int64) float64 {
	return mS.getSum(txnId)
}

func (mS *MockService) ListByType(txnType string) []transactions.Transaction {
	return mS.listByType(txnType)
}

type httpResp struct {
	Status  int
	Body    string
	Headers http.Header
}

func assertResponse(t *testing.T, want httpResp, got *http.Response) {
	defer got.Body.Close()

	assert.Equal(t, want.Status, got.StatusCode)
	if len(want.Headers) > 0 {
		assert.Equal(t, want.Headers, got.Header)
	}

	var bodyStr string
	if got.Body != nil {
		responseBody, err := ioutil.ReadAll(got.Body)
		require.NoError(t, err)
		bodyStr = strings.TrimSpace(string(responseBody))
	}

	assert.JSONEq(t, want.Body, bodyStr)
}

func TestCreateOrUpdate(t *testing.T) {
	t.Parallel()

	table := []struct {
		title         string
		transactionId string
		reqBody       string
		svc           TransactionService
		want          httpResp
	}{
		{
			title:   "CorruptRequestBody",
			reqBody: "invalid-body",
			want: httpResp{
				Status: http.StatusBadRequest,
				Body:   errResp(ErrBadReq(errors.New("failed to decode body: invalid character 'i' looking for beginning of value"))),
			},
		},
		{
			title:         "PathParam-InvalidTransactionId",
			transactionId: "A",
			reqBody: `{
				"amount":   0.0,
				"type":     "",
				"parent_id": 0
			}`,
			want: httpResp{
				Status: http.StatusBadRequest,
				Body:   errResp(ErrBadReq(ErrInvalidTransactionId)),
			},
		},
		{
			title:         "RequestBody-InvalidAmount",
			transactionId: "1",
			reqBody: `{
				"amount":   0,
				"type":     "",
				"parent_id": 0
			}`,
			want: httpResp{
				Status: http.StatusBadRequest,
				Body:   errResp(ErrBadReq(ErrInvalidAmount)),
			},
		},

		{
			title:         "RequestBody-InvalidType",
			transactionId: "1",
			reqBody: `{
				"amount":   10.9,
				"type":     "",
				"parent_id": 0
			}`,
			want: httpResp{
				Status: http.StatusBadRequest,
				Body:   errResp(ErrBadReq(ErrInvalidTransactionType)),
			},
		},

		{
			title:         "TransactionService-ExpectedError",
			transactionId: "1",
			reqBody: `{
				"amount":   10.9,
				"type":     "A",
				"parent_id": 0
			}`,
			svc: &MockService{
				createOrUpdate: func(txn transactions.Transaction) (*transactions.Transaction, error) {
					return nil, transactions.ErrCanNotUpdateParentID
				},
			},
			want: httpResp{
				Status: http.StatusBadRequest,
				Body:   errResp(ErrBadReq(transactions.ErrCanNotUpdateParentID)),
			},
		},

		{
			title:         "TransactionService-UnexpectedError",
			transactionId: "1",
			reqBody: `{
				"amount":   10.9,
				"type":     "A",
				"parent_id": 0
			}`,
			svc: &MockService{
				createOrUpdate: func(txn transactions.Transaction) (*transactions.Transaction, error) {
					return nil, errors.New("unexpected-error-occurred")
				},
			},
			want: httpResp{
				Status: http.StatusInternalServerError,
				Body:   errResp(ErrInternal()),
			},
		},

		{
			title:         "Success",
			transactionId: "1",
			reqBody: `{
				"amount":   10.9,
				"type":     "A",
				"parent_id": 0
			}`,
			svc: &MockService{
				createOrUpdate: func(txn transactions.Transaction) (*transactions.Transaction, error) {
					return &transactions.Transaction{
						ID:     1,
						Amount: 10.9,
						Kind:   "A",
					}, nil
				},
			},
			want: httpResp{
				Status: http.StatusCreated,
				Body: `{
					"data": {
						"id": 1,
						"amount":   10.9,
						"type":     "A"
					},
					"error": {}
				}`,
			},
		},
	}

	for _, tt := range table {
		t.Run(tt.title, func(t *testing.T) {
			transactionIdFromRequest = func(r *http.Request) string { return tt.transactionId }

			wr := httptest.NewRecorder()
			testReq := httptest.NewRequest(http.MethodPut, "/transaction_service/transactions/"+tt.transactionId, strings.NewReader(tt.reqBody))

			CreateOrUpdate(tt.svc)(wr, testReq)
			assertResponse(t, tt.want, wr.Result())
		})
	}
}
