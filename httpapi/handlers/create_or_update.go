package handlers

import (
	"errors"
	"github.com/gorilla/mux"
	"loco-app/domain/transactions"
	"net/http"
	"strconv"
	"strings"
)

var transactionIdFromRequest = func(r *http.Request) string {
	return mux.Vars(r)["transaction_id"]
}

type transactionRequestBody struct {
	ID       string  `json:"id"`
	Amount   float64 `json:"amount"`
	Kind     string  `json:"type"`
	ParentID int64   `json:"parent_id,omitempty"`
}

type transactionResponse struct {
	ID       int64  `json:"id"`
	Amount   float64 `json:"amount"`
	Kind     string  `json:"type"`
	ParentID int64   `json:"parent_id,omitempty"`
}

func (txnb *transactionRequestBody) Validate() error {
	id, err := strconv.ParseInt(txnb.ID, 10, 64)
	if err != nil {
		return ErrInvalidTransactionId
	}

	if id <= 0 {
		return ErrInvalidTransactionId
	}

	if txnb.Amount <= 0.0 {
		return errors.New("transaction amount should be greater than 0.0")
	}

	txnType := strings.TrimSpace(txnb.Kind)
	if txnType == "" {
		return ErrInvalidTransactionType
	}

	if txnb.ParentID < 0 {
		return errors.New("parentID should be valid positive integer")
	}

	return nil
}

func CreateOrUpdate(txnSvc TransactionService) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		txnId := transactionIdFromRequest(r)

		reqBody := transactionRequestBody{ID: txnId}
		if err := readJson(r, &reqBody); err != nil {
			respond(w, http.StatusBadRequest, ErrBadReq(err))
			return
		}

		createdOrUpdatedTransaction, err := txnSvc.CreateOrUpdate(reqBody.mapToTransaction())
		if err != nil {
			if err == transactions.ErrCanNotUpdateParentID ||
				err == transactions.ErrCanNotUpdateAmount ||
				err == transactions.ErrParentIDCanNotBeSameAsTxnID ||
				err == transactions.ErrParentNotFound {
				respond(w, http.StatusBadRequest, ErrBadReq(err))
				return
			}

			respond(w, http.StatusInternalServerError, ErrInternal())
			return
		}

		respond(w, http.StatusCreated, mapFromTransaction(*createdOrUpdatedTransaction))
	}
}

func (txnb *transactionRequestBody) mapToTransaction() transactions.Transaction {
	id, _ := strconv.ParseInt(txnb.ID, 10, 64)
	return transactions.Transaction{
		ID:       id,
		Amount:   txnb.Amount,
		Kind:     txnb.Kind,
		ParentID: txnb.ParentID,
	}
}
