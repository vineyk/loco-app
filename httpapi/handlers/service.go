package handlers

import (
	"loco-app/domain/transactions"
)

type TransactionService interface {
	CreateOrUpdate(txn transactions.Transaction) (*transactions.Transaction, error)
	GetById(txnId int64) (*transactions.Transaction, error)
	GetSum(txnId int64) float64								// <-- Will gather amounts from all the transactions
	ListByType(txnType string) []transactions.Transaction   	// <-- Not supporting pagination
}
