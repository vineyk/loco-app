package handlers

import (
	"encoding/json"
	"errors"
	"fmt"
	"loco-app/domain/transactions"
	"net/http"
	"strings"
)

var (
	ErrInvalidTransactionType = errors.New("invalid transaction type")
	ErrInvalidTransactionId   = errors.New("transactionID should be valid positive integer")
	ErrInvalidAmount          = errors.New("transaction amount should be greater than 0.0")
)

func mapFromTransaction(txn transactions.Transaction) transactionResponse {
	return transactionResponse{
		ID:       txn.ID,
		Amount:   txn.Amount,
		Kind:     txn.Kind,
		ParentID: txn.ParentID,
	}
}

func writeJson(wr http.ResponseWriter, status int, v interface{}) {
	wr.Header().Set("Content-type", "application/json; charset=utf-8")
	wr.WriteHeader(status)

	if err := json.NewEncoder(wr).Encode(v); err != nil {
		panic(fmt.Errorf("writeJSON failed: %w", err))
	}
}

func respond(wr http.ResponseWriter, status int, v interface{}) {
	var resp struct {
		Data  interface{} `json:"data,omitempty"`
		Error apiError    `json:"error,omitempty"`
	}

	switch val := v.(type) {
	case apiError:
		resp.Error = val

	default:
		resp.Data = v
	}

	writeJson(wr, status, resp)
}

func readJson(req *http.Request, into interface{}) error {
	if err := json.NewDecoder(req.Body).Decode(into); err != nil {
		return fmt.Errorf("failed to decode body: %v", err)
	}

	if v, ok := into.(interface{ Validate() error }); ok {
		return v.Validate()
	}

	return nil
}

func errResp(err apiError) string {
	return jsonStr(map[string]interface{}{
		"error": err,
	})
}

func jsonStr(v interface{}) string {
	var s strings.Builder
	if err := json.NewEncoder(&s).Encode(v); err != nil {
		panic(err)
	}
	return s.String()
}
