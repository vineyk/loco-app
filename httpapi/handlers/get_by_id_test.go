package handlers

import (
	"errors"
	"loco-app/domain/transactions"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestGetById(t *testing.T) {
	t.Parallel()

	table := []struct {
		title         string
		transactionId string
		svc           TransactionService
		want          httpResp
	}{
		{
			title:         "PathParam-InvalidTransactionId",
			transactionId: "A",
			want: httpResp{
				Status: http.StatusBadRequest,
				Body:   errResp(ErrBadReq(ErrInvalidTransactionId)),
			},
		},
		{
			title:         "PathParam-NegativeTransactionId",
			transactionId: "-1",
			want: httpResp{
				Status: http.StatusBadRequest,
				Body:   errResp(ErrBadReq(ErrInvalidTransactionId)),
			},
		},

		{
			title:         "TransactionService-ExpectedError",
			transactionId: "1",
			svc: &MockService{
				getById: func(txnID int64) (*transactions.Transaction, error) {
					return nil, transactions.ErrNotFound
				},
			},
			want: httpResp{
				Status: http.StatusNotFound,
				Body:   errResp(ErrNotFound("Not-Found", "Transaction with requested-id do not exists")),
			},
		},

		{
			title:         "TransactionService-UnexpectedError",
			transactionId: "1",
			svc: &MockService{
				getById: func(txnID int64) (*transactions.Transaction, error) {
					return nil, errors.New("unexpected-error-occurred")
				},
			},
			want: httpResp{
				Status: http.StatusInternalServerError,
				Body:   errResp(ErrInternal()),
			},
		},

		{
			title:         "Success",
			transactionId: "1",
			svc: &MockService{
				getById: func(txnId int64) (*transactions.Transaction, error) {
					return &transactions.Transaction{
						ID:     txnId,
						Amount: 10.9,
						Kind:   "A",
					}, nil
				},
			},
			want: httpResp{
				Status: http.StatusOK,
				Body: `{
					"data": {
						"id": 1,
						"amount":   10.9,
						"type":     "A"
					},
					"error": {}
				}`,
			},
		},
	}

	for _, tt := range table {
		t.Run(tt.title, func(t *testing.T) {
			transactionIdFromRequest = func(r *http.Request) string { return tt.transactionId }

			wr := httptest.NewRecorder()
			testReq := httptest.NewRequest(http.MethodGet, "/transaction_service/transactions/"+tt.transactionId, nil)

			GetById(tt.svc)(wr, testReq)
			assertResponse(t, tt.want, wr.Result())
		})
	}
}
