package handlers

import (
	"net/http"
	"strconv"
)

func GetSum(txnSvc TransactionService) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		txnId := transactionIdFromRequest(r)

		id, err := strconv.ParseInt(txnId, 10, 64)
		if err != nil {
			respond(w, http.StatusBadRequest, ErrBadReq(ErrInvalidTransactionId))
			return
		}

		if id <= 0 {
			respond(w, http.StatusBadRequest, ErrBadReq(ErrInvalidTransactionId))
			return
		}

		txnSum := txnSvc.GetSum(id)
		respond(w, http.StatusOK, mapToSumResp(txnSum))
	}
}

type SumResp struct {
	Sum float64 `json:"sum"`
}

func mapToSumResp(sum float64) SumResp {
	return SumResp{Sum: sum}
}
