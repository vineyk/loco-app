package handlers

import (
	"github.com/gorilla/mux"
	"loco-app/domain/transactions"
	"net/http"
	"strings"
)

var transactionTypeFromRequest = func(r *http.Request) string {
	return mux.Vars(r)["type"]
}

func ListByType(txnSvc TransactionService) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		txnType := strings.TrimSpace(transactionTypeFromRequest(r))
		if txnType == "" {
			respond(w, http.StatusBadRequest, ErrBadReq(ErrInvalidTransactionType))
			return
		}

		txnsList := txnSvc.ListByType(txnType)
		respond(w, http.StatusOK, mapFromTransactionsList(txnsList))
	}
}

func mapFromTransactionsList(txns []transactions.Transaction) []transactionResponse {
	response := make([]transactionResponse, 0)
	for _, txn := range txns {
		response = append(response, mapFromTransaction(txn))
	}

	return response
}
