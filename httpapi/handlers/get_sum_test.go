package handlers

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestGetSum(t *testing.T) {
	t.Parallel()

	table := []struct {
		title         string
		transactionId string
		svc           TransactionService
		want          httpResp
	}{
		{
			title:         "PathParam-InvalidTransactionId",
			transactionId: "A",
			want: httpResp{
				Status: http.StatusBadRequest,
				Body:   errResp(ErrBadReq(ErrInvalidTransactionId)),
			},
		},
		{
			title:         "PathParam-NegativeTransactionId",
			transactionId: "-1",
			want: httpResp{
				Status: http.StatusBadRequest,
				Body:   errResp(ErrBadReq(ErrInvalidTransactionId)),
			},
		},

		{
			title:         "Success",
			transactionId: "1",
			svc: &MockService{
				getSum: func(txnID int64) float64 {
					return 10.0
				},
			},
			want: httpResp{
				Status: http.StatusOK,
				Body: `{
					"data": {
						"sum": 10.0
					},
					"error": {}
				}`,
			},
		},
	}

	for _, tt := range table {
		t.Run(tt.title, func(t *testing.T) {
			transactionIdFromRequest = func(r *http.Request) string { return tt.transactionId }

			wr := httptest.NewRecorder()
			testReq := httptest.NewRequest(http.MethodGet, "/transaction_service/transactions/sum/"+tt.transactionId, nil)

			GetSum(tt.svc)(wr, testReq)
			assertResponse(t, tt.want, wr.Result())
		})
	}
}