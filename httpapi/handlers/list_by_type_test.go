package handlers

import (
	"loco-app/domain/transactions"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestListByType(t *testing.T) {
	t.Parallel()

	table := []struct {
		title           string
		transactionType string
		svc             TransactionService
		want            httpResp
	}{
		{
			title:           "PathParam-InvalidTransactionType",
			transactionType: "",
			want: httpResp{
				Status: http.StatusBadRequest,
				Body:   errResp(ErrBadReq(ErrInvalidTransactionType)),
			},
		},

		{
			title:           "Success-Empty",
			transactionType: "A",
			svc: &MockService{
				listByType: func(txnType string) []transactions.Transaction {
					return []transactions.Transaction{}
				},
			},
			want: httpResp{
				Status: http.StatusOK,
				Body: `{
					"data": [],
					"error": {}
				}`,
			},
		},

		{
			title:           "Success-Actual",
			transactionType: "B",
			svc: &MockService{
				listByType: func(txnType string) []transactions.Transaction {
					return []transactions.Transaction{
						{
							ID:     1,
							Amount: 10.9,
							Kind:   "B",
						},
						{
							ID:     2,
							Amount: 103.9,
							Kind:   "B",
						},
					}
				},
			},
			want: httpResp{
				Status: http.StatusOK,
				Body: `{
					"data": [
						{
							"id": 1,
							"amount":   10.9,
							"type":     "B"
						},
						{
							"id": 2,
							"amount":   103.9,
							"type":     "B"
						}
					],
					"error": {}
				}`,
			},
		},
	}

	for _, tt := range table {
		t.Run(tt.title, func(t *testing.T) {
			transactionTypeFromRequest = func(r *http.Request) string { return tt.transactionType }

			wr := httptest.NewRecorder()
			testReq := httptest.NewRequest(http.MethodGet, "/transaction_service/transactions/types/"+tt.transactionType, nil)

			ListByType(tt.svc)(wr, testReq)
			assertResponse(t, tt.want, wr.Result())
		})
	}
}
