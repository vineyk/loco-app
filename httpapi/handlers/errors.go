package handlers

const (
	errBadRequest = "bad_request"
	errInternal   = "internal_error"
)

type apiError struct {
	Code    string `json:"code,omitempty"`
	Message string `json:"message,omitempty"`
	Title   string `json:"message_title,omitempty"`
	Reason  string `json:"reason,omitempty"`
}

func ErrInternal() apiError {
	return apiError{
		Code:    errInternal,
		Title:   "Internal Error",
		Message: "Something went wrong",
	}
}

func ErrNotFound(title, message string) apiError {
	return apiError{
		Code:    errInternal,
		Title:   title,
		Message: message,
	}
}

func ErrBadReq(err error) apiError {
	return apiError{
		Code:    errBadRequest,
		Message: "Please check the request structure",
		Title:   "Bad Request",
		Reason:  err.Error(),
	}
}
