package handlers

import (
	"loco-app/domain/transactions"
	"net/http"
	"strconv"
)

func GetById(txnSvc TransactionService) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		txnId := transactionIdFromRequest(r)

		id, err := strconv.ParseInt(txnId, 10, 64)
		if err != nil {
			respond(w, http.StatusBadRequest, ErrBadReq(ErrInvalidTransactionId))
			return
		}

		if id <= 0 {
			respond(w, http.StatusBadRequest, ErrBadReq(ErrInvalidTransactionId))
			return
		}

		txn, err := txnSvc.GetById(id)
		if err != nil {
			if err == transactions.ErrNotFound {
				respond(w, http.StatusNotFound, ErrNotFound("Not-Found", "Transaction with requested-id do not exists"))
				return
			}

			respond(w, http.StatusInternalServerError, ErrInternal())
			return
		}

		respond(w, http.StatusOK, mapFromTransaction(*txn))
	}
}
