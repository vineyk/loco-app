package transactions

import (
	"github.com/stretchr/testify/assert"
	"loco-app/domain/transactions"
	"testing"
)

func TestTxnStore_Create(t *testing.T) {
	t.Parallel()

	t.Run("should create a New transaction record", func(t *testing.T) {
		store := NewStore()
		mockTransaction := transactions.Transaction{
			ID:     1,
			Amount: 100,
			Kind:   "A",
		}

		txn, err := store.Create(mockTransaction)
		assert.NoError(t, err)

		mockTransaction.Sum = 100
		assert.Equal(t, txn, &mockTransaction)
	})

	t.Run("should return error in case transaction already exists with same ID", func(t *testing.T) {
		store := NewStore()
		mockTransaction := transactions.Transaction{
			ID:     2,
			Amount: 100,
			Kind:   "A",
		}

		_, err := store.Create(mockTransaction)
		assert.NoError(t, err)

		_, err = store.Create(mockTransaction)
		assert.EqualError(t, err, "transaction already exist")
	})
}

func TestTxnStore_Update(t *testing.T) {
	t.Parallel()

	t.Run("should return error if txn do not exists", func(t *testing.T) {
		store := NewStore()
		mockTransaction := transactions.Transaction{
			ID:     1,
			Amount: 100,
			Kind:   "A",
		}

		_, err := store.Update(mockTransaction)
		assert.EqualError(t, err, transactions.ErrNotFound.Error())
	})

	t.Run("should update the transaction record", func(t *testing.T) {
		store := NewStore()
		mockTransaction := transactions.Transaction{
			ID:     1,
			Amount: 100,
			Kind:   "A",
		}

		_, err := store.Create(mockTransaction)
		assert.NoError(t, err)

		mockTransaction = transactions.Transaction{
			ID:     1,
			Amount: 105,
			Kind:   "B",
		}

		txn, err := store.Update(mockTransaction)
		assert.NoError(t, err)

		// Not asserting Sum and Amount; since they will not be allowed to be updated.
		assert.Equal(t, txn, &mockTransaction)
	})
}

func TestTxnStore_GetSum(t *testing.T) {
	t.Parallel()
	t.Run("should return 0.0 in case transactionId do no exists", func(t *testing.T) {
		store := NewStore()
		sum := store.GetSum(201)
		assert.Equal(t, sum, 0.0)
	})

	t.Run("should return accumulated sum in case transaction are dependent", func(t *testing.T) {
		store := NewStore()
		mockTransaction1 := transactions.Transaction{
			ID:     101,
			Amount: 100,
			Kind:   "A",
		}

		mockTransaction2 := transactions.Transaction{
			ID:       102,
			Amount:   150,
			Kind:     "A",
			ParentID: 101,
		}

		mockTransaction3 := transactions.Transaction{
			ID:       103,
			Amount:   113,
			Kind:     "A",
			ParentID: 101,
		}

		_, err := store.Create(mockTransaction1)
		assert.NoError(t, err)

		_, err = store.Create(mockTransaction2)
		assert.NoError(t, err)

		_, err = store.Create(mockTransaction3)
		assert.NoError(t, err)

		sum := store.GetSum(101)
		assert.Equal(t, sum, 363.0)
	})
}

func TestTxnStore_GetById(t *testing.T) {
	t.Parallel()
	t.Run("should return not found error if txn do not exists", func(t *testing.T) {
		store := NewStore()
		_, err := store.GetById(1)
		assert.EqualError(t, err, transactions.ErrNotFound.Error())
	})

	t.Run("should return txn if exists exists", func(t *testing.T) {
		store := NewStore()
		mockTransaction := transactions.Transaction{
			ID:     501,
			Amount: 100,
			Kind:   "A",
		}

		_, err := store.Create(mockTransaction)
		assert.NoError(t, err)

		txn, err := store.GetById(501)
		assert.NoError(t, err)

		mockTransaction.Sum = 100
		assert.Equal(t, txn, &mockTransaction)
	})
}

func TestTxnStore_ListByType(t *testing.T) {
	t.Parallel()
	t.Run("should return empty list in case no record exists in DB", func(t *testing.T) {
		store := NewStore()
		txns := store.ListByType("A")

		expeted := make([]transactions.Transaction, 0)
		assert.Equal(t, expeted, txns)
	})

	t.Run("should return proper list in case record exists in DB", func(t *testing.T) {
		store := NewStore()
		mockTransaction1 := transactions.Transaction{
			ID:     1001,
			Amount: 100,
			Kind:   "A",
		}

		mockTransaction2 := transactions.Transaction{
			ID:       1002,
			Amount:   150,
			Kind:     "A",
			ParentID: 1001,
		}

		mockTransaction3 := transactions.Transaction{
			ID:       1003,
			Amount:   113,
			Kind:     "C",
			ParentID: 1002,
		}

		_, err := store.Create(mockTransaction1)
		assert.NoError(t, err)

		_, err = store.Create(mockTransaction2)
		assert.NoError(t, err)

		_, err = store.Create(mockTransaction3)
		assert.NoError(t, err)

		txns := store.ListByType("A")
		assert.Equal(t, txns, []transactions.Transaction{
			transactions.Transaction{
				ID:       1001,
				Amount:   100,
				Kind:     "A",
				ParentID: 0,
				Sum:      363,
			},
			transactions.Transaction{
				ID:       1002,
				Amount:   150,
				Kind:     "A",
				ParentID: 1001,
				Sum:      263,
			},
		})
	})
}
