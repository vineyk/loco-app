package transactions

import (
	"loco-app/domain/transactions"
	"sync"
)

type TransactionSum struct {
	id  int64
	sum float64
}

type TxnStore struct {
	sync.Mutex
	transactions map[int64]*transactions.Transaction
}

func NewStore() *TxnStore {
	txns := make(map[int64]*transactions.Transaction)
	return &TxnStore{
		transactions: txns,
	}
}

func (txs *TxnStore) updateSum(txnId int64, withAmount float64) {
	if txnId <= 0 {
		return
	}

	txn, ok := txs.transactions[txnId]
	if !ok {
		return
	}

	txn.Sum += withAmount

	// It'll be an infinite loop both txnId and parentId are same
	if txnId != txn.ParentID {
		txs.updateSum(txn.ParentID, withAmount)
	}
}

func (txs *TxnStore) Create(txn transactions.Transaction) (*transactions.Transaction, error) {
	txs.Lock()
	defer txs.Unlock()

	_, ok := txs.transactions[txn.ID]
	if ok {
		return &txn, transactions.ErrAlreadyExists
	}

	txs.transactions[txn.ID] = &txn
	txs.updateSum(txn.ID, txn.Amount)
	return &txn, nil
}

func (txs *TxnStore) Update(txn transactions.Transaction) (*transactions.Transaction, error) {
	txs.Lock()
	defer txs.Unlock()

	_, ok := txs.transactions[txn.ID]
	if !ok {
		return &txn, transactions.ErrNotFound
	}

	txs.transactions[txn.ID] = &txn
	return &txn, nil
}

func (txs *TxnStore) GetById(txnId int64) (*transactions.Transaction, error) {
	txs.Lock()
	defer txs.Unlock()

	transaction, ok := txs.transactions[txnId]
	if !ok {
		return nil, transactions.ErrNotFound
	}

	return transaction, nil
}

func (txs *TxnStore) GetSum(txnId int64) float64 {
	txs.Lock()
	defer txs.Unlock()

	transaction, ok := txs.transactions[txnId]
	if !ok {
		return 0.0
	}

	return transaction.Sum
}

func (txs *TxnStore) ListByType(txnType string) []transactions.Transaction {
	return txs.getByType(txnType)
}

func (txs *TxnStore) getByType(txnType string) []transactions.Transaction {
	txs.Lock()
	defer txs.Unlock()

	trxns := make([]transactions.Transaction, 0)
	for _, value := range txs.transactions {
		if value.Kind == txnType {
			trxns = append(trxns, *value)
		}
	}

	return trxns
}
