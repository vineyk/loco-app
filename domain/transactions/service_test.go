package transactions

import (
	"errors"
	"github.com/stretchr/testify/assert"
	"testing"
)

type MockStore struct{}

var create func(txn Transaction) (*Transaction, error)
var update func(txn Transaction) (*Transaction, error)
var getById func(txnId int64) (*Transaction, error)
var getSum func(txnId int64) float64
var listByType func(txnType string) []Transaction

func (mS *MockStore) Create(txn Transaction) (*Transaction, error) {
	return create(txn)
}

func (mS *MockStore) Update(txn Transaction) (*Transaction, error) {
	return update(txn)
}

func (mS *MockStore) GetById(txnId int64) (*Transaction, error) {
	return getById(txnId)
}

func (mS *MockStore) GetSum(txnId int64) float64 {
	return getSum(txnId)
}

func (mS *MockStore) ListByType(txnType string) []Transaction {
	return listByType(txnType)
}

func TestService_CreateOrUpdate(t *testing.T) {
	t.Parallel()

	t.Run("should return Error in case txn ID same as parent ID", func(t *testing.T) {
		service := NewService(&MockStore{})
		mockTransaction := Transaction{
			ID:       101,
			Amount:   100,
			Kind:     "A",
			ParentID: 101,
		}

		_, err := service.CreateOrUpdate(mockTransaction)
		assert.EqualError(t, err, ErrParentIDCanNotBeSameAsTxnID.Error())
	})

	t.Run("should return Error in case parent txn not found", func(t *testing.T) {
		service := NewService(&MockStore{})
		mockTransaction := Transaction{
			ID:       102,
			Amount:   100,
			Kind:     "A",
			ParentID: 105,
		}

		getById = func(txnId int64) (*Transaction, error) {
			return nil, ErrNotFound
		}

		_, err := service.CreateOrUpdate(mockTransaction)
		assert.EqualError(t, err, ErrParentNotFound.Error())
	})

	t.Run("should return Error in case getById returns unexpected error", func(t *testing.T) {
		service := NewService(&MockStore{})
		mockTransaction := Transaction{
			ID:       102,
			Amount:   100,
			Kind:     "A",
			ParentID: 105,
		}

		getById = func(txnId int64) (*Transaction, error) {
			return nil, errors.New("some-db error occurred")
		}

		_, err := service.CreateOrUpdate(mockTransaction)
		assert.EqualError(t, err, "some-db error occurred")
	})

	t.Run("should return error in case create returns error", func(t *testing.T) {
		service := NewService(&MockStore{})
		mockTransaction := Transaction{
			ID:       102,
			Amount:   100,
			Kind:     "A",
			ParentID: 105,
		}

		getById = func(txnId int64) (*Transaction, error) {
			if txnId == 105 {
				return nil, nil
			}

			return nil, ErrNotFound
		}

		create = func(txn Transaction) (*Transaction, error) {
			return &mockTransaction, errors.New("some-error occurred")
		}

		_, err := service.CreateOrUpdate(mockTransaction)
		assert.EqualError(t, err, "some-error occurred")
	})

	t.Run("should create a record if txnId do not exists", func(t *testing.T) {
		service := NewService(&MockStore{})
		mockTransaction := Transaction{
			ID:       102,
			Amount:   100,
			Kind:     "A",
			ParentID: 105,
		}

		getById = func(txnId int64) (*Transaction, error) {
			if txnId == 105 {
				return nil, nil
			}

			return nil, ErrNotFound
		}

		create = func(txn Transaction) (*Transaction, error) {
			return &mockTransaction, nil
		}

		txn, err := service.CreateOrUpdate(mockTransaction)
		assert.NoError(t, err)
		assert.Equal(t, &mockTransaction, txn)
	})

	t.Run("should return error in case parentId is being updated", func(t *testing.T) {
		service := NewService(&MockStore{})
		mockTransaction := Transaction{
			ID:       102,
			Amount:   100,
			Kind:     "A",
			ParentID: 105,
		}

		getById = func(txnId int64) (*Transaction, error) {
			mockTransaction := mockTransaction
			mockTransaction.ParentID = 5674
			return &mockTransaction, nil
		}

		_, err := service.CreateOrUpdate(mockTransaction)
		assert.EqualError(t, err, ErrCanNotUpdateParentID.Error())
	})

	t.Run("should return error in case Amount is being updated", func(t *testing.T) {
		service := NewService(&MockStore{})
		mockTransaction := Transaction{
			ID:       102,
			Amount:   100,
			Kind:     "A",
			ParentID: 105,
		}

		getById = func(txnId int64) (*Transaction, error) {
			mockTransaction := mockTransaction
			mockTransaction.Amount = 5674
			return &mockTransaction, nil
		}

		_, err := service.CreateOrUpdate(mockTransaction)
		assert.EqualError(t, err, ErrCanNotUpdateAmount.Error())
	})

	t.Run("should return error if update returns error", func(t *testing.T) {
		service := NewService(&MockStore{})
		mockTransaction := Transaction{
			ID:       102,
			Amount:   100,
			Kind:     "B",
			ParentID: 105,
		}

		getById = func(txnId int64) (*Transaction, error) {
			mockTransaction := mockTransaction
			mockTransaction.Kind = "A"
			return &mockTransaction, nil
		}

		update = func(txn Transaction) (*Transaction, error) {
			return nil, errors.New("some-error occurred")
		}

		_, err := service.CreateOrUpdate(mockTransaction)
		assert.EqualError(t, err, "some-error occurred")
	})

	t.Run("should update txn Successfully", func(t *testing.T) {
		service := NewService(&MockStore{})
		mockTransaction := Transaction{
			ID:       102,
			Amount:   100,
			Kind:     "B",
			ParentID: 105,
		}

		getById = func(txnId int64) (*Transaction, error) {
			mockTransaction := mockTransaction
			mockTransaction.Kind = "A"
			return &mockTransaction, nil
		}

		update = func(txn Transaction) (*Transaction, error) {
			return &mockTransaction, nil
		}

		txn, err := service.CreateOrUpdate(mockTransaction)
		assert.NoError(t, err)
		assert.Equal(t, &mockTransaction, txn)
	})
}
