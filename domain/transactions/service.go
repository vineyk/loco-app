package transactions

type Service struct {
	Store TransactionStore
}

func NewService(store TransactionStore) *Service {
	return &Service{Store: store}
}

func (txnSvc *Service) CreateOrUpdate(txn Transaction) (*Transaction, error) {
	if txn.ID == txn.ParentID {
		return nil, ErrParentIDCanNotBeSameAsTxnID
	}

	// If parent txn not found; return error.
	if txn.ParentID > 0 {
		_, err := txnSvc.Store.GetById(txn.ParentID)
		if err != nil {
			if err == ErrNotFound {
				return nil, ErrParentNotFound
			}

			return nil, err
		}
	}

	storeTxn, err := txnSvc.Store.GetById(txn.ID)

	// Create it if not found in DB
	if err != nil {
		if err == ErrNotFound {
			return txnSvc.Store.Create(txn)
		}

		return nil, err
	}

	// Update Validations
	if storeTxn.ParentID != txn.ParentID {
		return nil, ErrCanNotUpdateParentID
	}

	if storeTxn.Amount != txn.Amount {
		return nil, ErrCanNotUpdateAmount
	}

	return txnSvc.Store.Update(txn)
}

func (txnSvc *Service) GetById(txnId int64) (*Transaction, error) {
	return txnSvc.Store.GetById(txnId)
}

func (txnSvc *Service) GetSum(txnId int64) float64 {
	return txnSvc.Store.GetSum(txnId)
}

func (txnSvc *Service) ListByType(txnType string) []Transaction {
	return txnSvc.Store.ListByType(txnType)
}