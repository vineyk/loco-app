package transactions

import "errors"

var (
	ErrCanNotUpdateParentID        = errors.New("parentId can't be updated")
	ErrParentIDCanNotBeSameAsTxnID = errors.New("parentId can't be same as transactionID")
	ErrCanNotUpdateAmount          = errors.New("amount of the transaction can't be updated")
	ErrNotFound                    = errors.New("transaction not found")
	ErrParentNotFound                    = errors.New("parent transaction not found")
	ErrAlreadyExists               = errors.New("transaction already exist")
)

type Transaction struct {
	ID       int64
	Amount   float64
	Kind     string // <--- type is a keyword ... so using Kind instead
	ParentID int64
	Sum      float64
}

type TransactionStore interface {
	Create(txn Transaction) (*Transaction, error)
	Update(txn Transaction) (*Transaction, error)
	GetById(txnId int64) (*Transaction, error)
	GetSum(txnId int64) float64
	ListByType(txnType string) []Transaction
}
